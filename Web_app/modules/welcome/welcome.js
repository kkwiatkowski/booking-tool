Devices = new Mongo.Collection('devices');

if (Meteor.isClient) {

  Template.welcome.helpers({

    name: Meteor.userId(),
    devices: function() {
      return Devices.find();
    }

  });

  Template.welcome.events({

    'click .submit-button': function(e) {
      e.preventDefault();

      var device = $('.new-device').val();
      var status = $('.new-status').val();

      Devices.insert({
        device: device,
        status: status,
      })

    }

  })

}

if (Meteor.isServer) {

}

// database 1 - devices

var Devices = [{
  device: "iPhone 4",
  os: "iOS 4",
  category: "Test device",
  id: "MOB2",
  brand: "Apple",
  status: "BOOKED"
}, {
  device: "iPhone 4",
  os: "iOS 4",
  category: "Test device",
  id: "MOB2",
  brand: "Apple"
}, {
  device: "iPhone 4",
  os: "iOS 4",
  category: "Test device",
  id: "MOB2",
  brand: "Apple"
}]

// database 2 - booking system

var Bookings = [{
  device: "MOB2",
  userId: "MnaMhKvsyCesMCqD3",
  bookingStart: "14-3-18:83432:23423",
  bookingDue: "14-3-18:83432:23423",
  bookingStatus: "RETURNED"
}]